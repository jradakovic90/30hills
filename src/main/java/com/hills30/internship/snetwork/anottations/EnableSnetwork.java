package com.hills30.internship.snetwork.anottations;

import com.hills30.internship.snetwork.config.FeignMappingDefaultConfiguration;
import com.hills30.internship.snetwork.config.SwaggerConfiguration;
import com.hills30.internship.snetwork.config.WebMvcConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({ SwaggerConfiguration.class, WebMvcConfiguration.class, FeignMappingDefaultConfiguration.class })
public @interface EnableSnetwork {

}
