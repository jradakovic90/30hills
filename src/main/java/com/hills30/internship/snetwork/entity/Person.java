package com.hills30.internship.snetwork.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long id;
		private String firstName;
		private String surname;
		private String gender;
		private int age;
		@Column
		@ElementCollection(targetClass = Long.class)
		private List<Long> friendsList;

		protected Person() {
		}

		public Person(String firstName, String surname, String gender, int age, List<Long> friendsList) {
				this.firstName = firstName;
				this.surname = surname;
				this.gender = gender;
				this.age = age;
				this.friendsList = friendsList;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getGender() {
				return gender;
		}

		public void setGender(String gender) {
				this.gender = gender;
		}

		public int getAge() {
				return age;
		}

		public void setAge(int age) {
				this.age = age;
		}

		public List<Long> getFriendsList() {
				return friendsList;
		}

		public void setFriendsList(List<Long> friendsList) {
				this.friendsList = friendsList;
		}

}
