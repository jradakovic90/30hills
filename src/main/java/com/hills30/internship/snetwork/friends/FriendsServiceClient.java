package com.hills30.internship.snetwork.friends;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "friends", url = "${feign.friends.url}")
public interface FriendsServiceClient extends FriendsService {

}
