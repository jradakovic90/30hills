package com.hills30.internship.snetwork.friends;

import com.hills30.internship.snetwork.dto.person.PersonDTO;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;

public interface FriendsService {

		@GetMapping("/direct")
		Resources<PersonDTO> getDirectFriends(Long personId);

		@GetMapping("/friends")
		Resources<PersonDTO> getFriendsOfFriends(Long personId);

		@GetMapping("/suggested")
		Resources<PersonDTO> getSuggestedFriends(Long personId);

		@GetMapping("/users")
		Resources<PersonDTO> getAllUsers();

}
