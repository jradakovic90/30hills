package com.hills30.internship.snetwork.repository;

import com.hills30.internship.snetwork.entity.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SNetworkRepository extends CrudRepository<Person, Long> {

		List<Person> findBySurname(String surname);

		List<Person> findAll();

		List<Person> findByIdIn(List<Long> friendsList);

		Person findPersonById(Long id);

		List<Person> findByIdNotIn(List<Long> friendsList);

}
