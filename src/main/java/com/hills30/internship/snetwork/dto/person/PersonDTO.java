package com.hills30.internship.snetwork.dto.person;

import com.hills30.internship.snetwork.entity.Person;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.util.List;

public class PersonDTO extends ResourceSupport implements Serializable {

		@ApiModelProperty(notes = "First name of the person")
		private String firstName;

		@ApiModelProperty(notes = "Last name of the person")
		private String surname;

		@ApiModelProperty(notes = "Gender of the person")
		private String gender;

		@ApiModelProperty(notes = "Age of the person")
		private int age;

		@ApiModelProperty(notes = "List containing ids of the persons friends")
		private List<Integer> friendsList;

		private final Person person;

		public PersonDTO(Person person) {
				this.person = person;
		}

		public Person getPerson() {
				return person;
		}
}
