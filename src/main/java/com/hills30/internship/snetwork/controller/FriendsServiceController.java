package com.hills30.internship.snetwork.controller;

import com.hills30.internship.snetwork.repository.SNetworkRepository;
import com.hills30.internship.snetwork.entity.Person;
import com.hills30.internship.snetwork.friends.FriendsService;
import com.hills30.internship.snetwork.dto.person.PersonDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
@Api(value = "friends", description = "Getting friends for the selected user")
public class FriendsServiceController implements FriendsService {

		private static Logger log;

		@Autowired
		private SNetworkRepository sNetworkRepository;

		@ApiOperation(value = "Getting friends for selected person", response = PersonDTO.class)

		@Override
		public Resources<PersonDTO> getDirectFriends(Long personId) {

				if (sNetworkRepository.count() == 0) {
						createAllUsers();
				}
				List<Person> allPersons = sNetworkRepository.findAll();

				Person selectedUser = sNetworkRepository.findPersonById(personId);
				List<Person> directFriends = sNetworkRepository.findByIdIn(selectedUser.getFriendsList());

				final List<PersonDTO> directFriendsList = directFriends.stream().map(PersonDTO::new)
						.collect(Collectors.toList());

				return new Resources<>(directFriendsList);

		}

		@Override
		public Resources<PersonDTO> getFriendsOfFriends(Long personId) {

				if (sNetworkRepository.count() == 0) {
						createAllUsers();
				}

				Person selectedUser = sNetworkRepository.findPersonById(personId);

				List<Person> friendsOfFriends = findFriendsOfFriends(selectedUser);

				final List<PersonDTO> friendsOfFriendsList = friendsOfFriends.stream().map(PersonDTO::new)
						.collect(Collectors.toList());

				return new Resources<>(friendsOfFriendsList);
		}

		private List<Person> findFriendsOfFriends(Person selectedUser) {
				List<Long> friendsList = selectedUser.getFriendsList();
				List<Long> ownList = new ArrayList<>();
				ownList.addAll(friendsList);
				ownList.add(selectedUser.getId());

				return sNetworkRepository.findByIdNotIn(ownList);
		}

		@Override
		public Resources<PersonDTO> getSuggestedFriends(Long personId) {

				if (sNetworkRepository.count() == 0) {
						createAllUsers();
				}

				Person selectedUser = sNetworkRepository.findPersonById(personId);

				List<Person> friendsOfFriends = this.findFriendsOfFriends(selectedUser);
				List<Long> directFriends = selectedUser.getFriendsList();
				int numberOfKnownFoF = 0;
				List<Person> suggestedFriends = new ArrayList<Person>();
				for (Person p : friendsOfFriends) {
						List<Long> friendsList = p.getFriendsList();
						for (Long id : friendsList) {
								if (directFriends.contains(id)) {
										numberOfKnownFoF++;
								}
								if (numberOfKnownFoF > 1) {
										suggestedFriends.add(p);
										break;
								}
						}
						numberOfKnownFoF = 0;
				}

				final List<PersonDTO> suggestedFriendsList = suggestedFriends.stream().map(PersonDTO::new)
						.collect(Collectors.toList());

				return new Resources<>(suggestedFriendsList);
		}

		@Override public Resources<PersonDTO> getAllUsers() {
				if (sNetworkRepository.count() == 0) {
						createAllUsers();
				}

				final List<PersonDTO> allUsers = sNetworkRepository.findAll().stream().map(PersonDTO::new)
						.collect(Collectors.toList());

				return new Resources<>(allUsers);
		}

		private void createAllUsers() {
				List<Long> paulFriends = new ArrayList<Long>();
				paulFriends.add(2l);
				this.createFriends("Paul", "Crowe", 28, "male", paulFriends);

				List<Long> robFriends = new ArrayList<Long>();
				robFriends.add(1l);
				robFriends.add(3l);
				this.createFriends("Rob", "Fitz", 23, "male", robFriends);

				List<Long> benFriends = new ArrayList<Long>();
				benFriends.add(2l);
				benFriends.add(4l);
				benFriends.add(5l);
				benFriends.add(7l);
				this.createFriends("Ben", "O'Carolan", 0, "male", benFriends);

				List<Long> victorFriends = new ArrayList<Long>();
				victorFriends.add(3l);
				this.createFriends("Victor", "", 28, "male", victorFriends);

				List<Long> peterFriends = new ArrayList<Long>();
				peterFriends.add(3l);
				peterFriends.add(6l);
				peterFriends.add(11l);
				peterFriends.add(10l);
				peterFriends.add(7l);
				this.createFriends("Peter", "Mac", 29, "male", peterFriends);

				List<Long> johnFriends = new ArrayList<Long>();
				johnFriends.add(5l);
				this.createFriends("John", "Barry", 18, "male", johnFriends);

				List<Long> sarahFriends = new ArrayList<Long>();
				sarahFriends.add(3l);
				sarahFriends.add(5l);
				sarahFriends.add(20l);
				sarahFriends.add(12l);
				sarahFriends.add(8l);
				this.createFriends("Sarah", "Lane", 30, "female", sarahFriends);

				List<Long> susanFriends = new ArrayList<Long>();
				susanFriends.add(7l);
				this.createFriends("Susan", "Downe", 28, "female", susanFriends);

				List<Long> jackFriends = new ArrayList<Long>();
				jackFriends.add(12l);
				this.createFriends("Jack", "Stam", 28, "male", jackFriends);

				List<Long> amyFriends = new ArrayList<Long>();
				amyFriends.add(5l);
				amyFriends.add(11l);
				this.createFriends("Amy", "Lane", 24, "female", amyFriends);

				List<Long> sandraFriends = new ArrayList<Long>();
				sandraFriends.add(5l);
				sandraFriends.add(10l);
				sandraFriends.add(19l);
				sandraFriends.add(20l);
				this.createFriends("Sandra", "Phelan", 28, "female", sandraFriends);

				List<Long> lauraFriends = new ArrayList<Long>();
				lauraFriends.add(7l);
				lauraFriends.add(9l);
				lauraFriends.add(13l);
				lauraFriends.add(20l);
				this.createFriends("Laura", "Murphy", 33, "female", lauraFriends);

				List<Long> lisaFriends = new ArrayList<Long>();
				lisaFriends.add(12l);
				lisaFriends.add(14l);
				lisaFriends.add(20l);
				this.createFriends("Lisa", "Daly", 28, "female", lisaFriends);

				List<Long> markFriends = new ArrayList<Long>();
				markFriends.add(13l);
				markFriends.add(15l);
				this.createFriends("Mark", "Johnson", 28, "male", markFriends);

				List<Long> seamusFriends = new ArrayList<Long>();
				seamusFriends.add(14l);
				this.createFriends("Seamus", "Crowe", 24, "male", seamusFriends);

				List<Long> darenFriends = new ArrayList<Long>();
				darenFriends.add(18l);
				darenFriends.add(20l);
				this.createFriends("Daren", "Slater", 28, "male", darenFriends);

				List<Long> daraFriends = new ArrayList<Long>();
				daraFriends.add(18l);
				daraFriends.add(20l);
				this.createFriends("Dara", "Zoltan", 48, "male", daraFriends);

				List<Long> marieFriends = new ArrayList<Long>();
				marieFriends.add(17l);
				this.createFriends("Marie", "D", 28, "female", marieFriends);

				List<Long> catrionaFriends = new ArrayList<Long>();
				catrionaFriends.add(11l);
				catrionaFriends.add(20l);
				this.createFriends("Catriona", "Long", 28, "female", catrionaFriends);

				List<Long> katyFriends = new ArrayList<Long>();
				katyFriends.add(7l);
				katyFriends.add(11l);
				katyFriends.add(12l);
				katyFriends.add(13l);
				katyFriends.add(16l);
				katyFriends.add(17l);
				katyFriends.add(19l);
				this.createFriends("Katy", "Couch", 28, "female", katyFriends);
		}

		private void createFriends(String firstName, String surname, int age, String gender,
				List<Long> friendsList) {
				sNetworkRepository.save(new Person(firstName, surname, gender, age, friendsList));
		}

}
