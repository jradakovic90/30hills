package com.hills30.internship.snetwork.config;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrations;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Configuration
@ConditionalOnClass({ Feign.class })
@EnableFeignClients(basePackages = "com.hills30.internship")
public class FeignMappingDefaultConfiguration {

		@Bean
		public WebMvcRegistrations feignWebRegistrations() {

				return new WebMvcRegistrationsAdapter() {

						@Override
						public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {

								return new FeignFilterRequestMappingHandlerMapping();
						}
				};
		}

		private static class FeignFilterRequestMappingHandlerMapping extends RequestMappingHandlerMapping {

				@Override
				protected boolean isHandler(Class<?> beanType) {

						return super.isHandler(beanType) && (AnnotationUtils.findAnnotation(beanType, FeignClient.class)
								== null);
				}
		}
}
