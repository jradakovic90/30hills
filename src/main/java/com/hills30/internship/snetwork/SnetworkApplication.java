package com.hills30.internship.snetwork;

import com.hills30.internship.snetwork.anottations.EnableSnetwork;
import com.hills30.internship.snetwork.config.FriendsConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableConfigurationProperties(FriendsConfiguration.class)
@ComponentScan(basePackages = { "com.hills30.internship" })
@EntityScan(basePackages = { "com.hills30.internship.snetwork.entity" })
@EnableJpaRepositories(basePackages = { "com.hills30.internship.snetwork.repository" })
@EnableSnetwork
public class SnetworkApplication {

		public static void main(String[] args) {
				SpringApplication.run(SnetworkApplication.class, args);
		}
}
